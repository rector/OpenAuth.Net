namespace OpenAuth.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModuleElementGrant",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ElementId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        GrantType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ModuleElement",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DomId = c.String(nullable: false, maxLength: 255),
                        Name = c.String(nullable: false, maxLength: 255),
                        Type = c.Int(nullable: false),
                        ModuleId = c.Int(nullable: false),
                        Remark = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Module",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CascadeId = c.String(nullable: false, maxLength: 255),
                        Name = c.String(nullable: false, maxLength: 255),
                        Url = c.String(nullable: false, maxLength: 255),
                        HotKey = c.String(nullable: false, maxLength: 255),
                        ParentId = c.Int(nullable: false),
                        IsLeaf = c.Boolean(nullable: false),
                        IsAutoExpand = c.Boolean(nullable: false),
                        IconName = c.String(nullable: false, maxLength: 255),
                        Status = c.Int(nullable: false),
                        ParentName = c.String(nullable: false, maxLength: 255),
                        Vector = c.String(nullable: false, maxLength: 255),
                        SortNo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Org",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CascadeId = c.String(nullable: false, maxLength: 255),
                        Name = c.String(nullable: false, maxLength: 255),
                        HotKey = c.String(nullable: false, maxLength: 255),
                        ParentId = c.Int(nullable: false),
                        ParentName = c.String(nullable: false, maxLength: 255),
                        IsLeaf = c.Boolean(nullable: false),
                        IsAutoExpand = c.Boolean(nullable: false),
                        IconName = c.String(nullable: false, maxLength: 255),
                        Status = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        BizCode = c.String(nullable: false, maxLength: 255),
                        CustomCode = c.String(nullable: false, maxLength: 4000),
                        CreateTime = c.DateTime(nullable: false),
                        CreateId = c.Int(nullable: false),
                        SortNo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Relevance",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstId = c.Int(nullable: false),
                        SecondId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 100),
                        Key = c.String(nullable: false, maxLength: 100),
                        Status = c.Int(nullable: false),
                        OperateTime = c.DateTime(nullable: false),
                        OperatorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        Status = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        CreateTime = c.DateTime(nullable: false),
                        CreateId = c.String(nullable: false, maxLength: 64),
                        OrgId = c.Int(nullable: false),
                        OrgCascadeId = c.String(nullable: false, maxLength: 255),
                        OrgName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserCfg",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Theme = c.String(nullable: false, maxLength: 255),
                        Skin = c.String(nullable: false, maxLength: 255),
                        NavBarStyle = c.String(nullable: false, maxLength: 255),
                        TabFocusColor = c.String(nullable: false, maxLength: 255),
                        NavTabIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserExt",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 255),
                        Phone_ = c.String(nullable: false, maxLength: 255),
                        Mobile = c.String(nullable: false, maxLength: 255),
                        Address = c.String(nullable: false, maxLength: 255),
                        Zip = c.String(nullable: false, maxLength: 255),
                        Birthday = c.String(nullable: false, maxLength: 255),
                        IdCard = c.String(nullable: false, maxLength: 255),
                        QQ = c.String(nullable: false, maxLength: 255),
                        DynamicField = c.String(nullable: false, maxLength: 4000),
                        ByteArrayId = c.Int(nullable: false),
                        Remark = c.String(nullable: false, maxLength: 4000),
                        Field1 = c.String(nullable: false, maxLength: 255),
                        Field2 = c.String(nullable: false, maxLength: 255),
                        Field3 = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Account = c.String(nullable: false, maxLength: 255),
                        Password = c.String(nullable: false, maxLength: 255),
                        Name = c.String(nullable: false, maxLength: 255),
                        Sex = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        BizCode = c.String(nullable: false, maxLength: 255),
                        CreateTime = c.DateTime(nullable: false),
                        CreateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.User");
            DropTable("dbo.UserExt");
            DropTable("dbo.UserCfg");
            DropTable("dbo.Role");
            DropTable("dbo.Relevance");
            DropTable("dbo.Org");
            DropTable("dbo.Module");
            DropTable("dbo.ModuleElement");
            DropTable("dbo.ModuleElementGrant");
        }
    }
}
