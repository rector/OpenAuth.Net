namespace OpenAuth.Repository.Migrations
{
    using Domain;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OpenAuth.Repository.Models.OpenAuthDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OpenAuth.Repository.Models.OpenAuthDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Users.AddOrUpdate(x => x.Id,
                new User { Id = 1, Account = "Administrator", Password = "admin", BizCode = "", CreateId = 0, CreateTime = DateTime.Now, Name = "超级管理员", Sex = 0, Status = 0, Type = 0 },
                new User { Id = 2, Account = "Admin", Password = "admin", BizCode = "", CreateId = 0, CreateTime = DateTime.Now, Name = "管理员", Sex = 0, Status = 0, Type = 0 }
                );
        }
    }
}
